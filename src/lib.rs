#[macro_export]
macro_rules! cenum {
    ($name:ident $typ:ty {
        $($variant:ident = $val:expr,)*
    }) => {
        #[derive(Eq, PartialEq, Clone, Copy)]
        pub struct $name(pub $typ);
        $(pub const $variant: $name = $name($val);)*
        
        impl ::std::fmt::Debug for $name {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                match *self {
                    $(
                        $variant => write!(f, "{}", stringify!($variant)),  
                    )*
                    _ => write!(f, "{}({})", stringify!($name), self.0)
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    cenum!{ col &'static str {
        Red = "red",
        Blue = "green",
    }}
    #[test]
    fn it_works() {
        assert!(Red != Blue);
        assert!(Red == Red);
    }
    
    #[test]
    fn arbitrary() {
        assert!(col("red") == Red);
    }
    
    #[test]
    fn printing() {
        println!("{:?}, {:?}", Red, col("Yellow"));
        assert!(format!("{:?}", Red) == "Red");
        assert!(format!("{:?}", col("red")) == "Red");
        assert!(format!("{:?}", col("Yellow")) == "col(Yellow)");
    }
}
